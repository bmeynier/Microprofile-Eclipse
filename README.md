# Eclipse Microprofile

[![pipeline status](https://gitlab.com/bmeynier/Microprofile-Eclipse/badges/master/pipeline.svg)](https://gitlab.com/bmeynier/Microprofile-Eclipse/commits/master)

Example with KumuluZee implementation and cloud configurations.

Check the article on http://baptiste-meynier.com/


