package com.bmeynier.article.microprofile.domain.enums;

public enum WaterType {
    SEA,FRESH
}
