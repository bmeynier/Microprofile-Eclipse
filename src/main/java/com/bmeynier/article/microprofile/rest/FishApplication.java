package com.bmeynier.article.microprofile.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/shop")
public class FishApplication extends Application {}
